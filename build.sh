#!/bin/sh
set -e

file_name=action-camera-swap.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths ${file_name} scripts CHANGELOG.md LICENSE README.md l10n action-camera-swap.omwscripts version.txt
sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
