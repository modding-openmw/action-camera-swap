# Action Camera Swap

A camera mod for OpenMW that auto-swaps between first and third person cameras based on if combat or magic are readied or if you're indoors or not.

If [Morrowind Interiors Project](https://www.nexusmods.com/morrowind/mods/52237) is used alongside this its exterior interiors are properly handled.

#### Credits

##### OpenMW-Lua author

johnnyhostile

##### Localizations

DE: Atahualpa

EN: johnnyhostile

SV: Lysol

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/action-camera-swap/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Camera\ActionCameraSwap

        # Linux
        /home/username/games/OpenMWMods/Camera/ActionCameraSwap

        # macOS
        /Users/username/games/OpenMWMods/Camera/ActionCameraSwap

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Camera\ActionCameraSwap"`)
1. Add `content=action-camera-swap.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher
1. Configure the mod from the Options >> Scripts menu, and enjoy!

Note that this mod works great with OpenMW's built-in **Smooth view change** camera feature!

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/action-camera-swap/-/issues)
* Email `action-camera-swap@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`

#### Planned Features

* A cinematic camera when conversations are started.
* [Request a feature!](https://gitlab.com/modding-openmw/action-camera-swap/-/issues/new)
